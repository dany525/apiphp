<?php
Header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
class Index 
{


    private $modulo = "";
    private $funcion = "";
 
    public function __construct($m,$f) {

            $this->modulo = $m;
            $this->funcion = $f;
       
    }

    public function redirecion(){

        include 'model/masterModel/masterModel.php';

        $metodo  = ''. $this->modulo.'Controlador';
        $funcion = $this->funcion;
 
       include 'controlador/'.$this->modulo.'/'.$this->modulo .'Controlador.php';
       $newController = new $metodo();
       $newController->$funcion();


    }

    
}
    
      $modulo  =    $_REQUEST['m'];
      $funcion =    $_REQUEST['f'];
      $index   =  new  Index($modulo,$funcion);
      $index->redirecion();

?>